import java.util.Scanner;

public class GradeBookTest
{

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		GradeBook mybook = new GradeBook("no name");
		GradeBook mybook2 = new GradeBook("Euclidian Geometry");
		String name;
		
		System.out.printf("The name of your current class is saved as \n%s\n", mybook.getCourseName());
		
		System.out.println("Please enter the name of your class for your grade book");
		name = input.nextLine();
		
		mybook.setCourseName(name);
		
		System.out.printf("The new name saved for your grade book is \n%s\n", mybook.getCourseName());
		
		mybook.displayMessage();
		System.out.println();
		mybook2.displayMessage();
		

	}

}
